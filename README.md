# ddns - Dynamic DNS Daemon

![build status](https://gitlab.com/ocaner/ddns/badges/master/pipeline.svg)

## TODO

* [ ] Better exit handling
* [ ] Log output dissappears
* [ ] No IPv6 in Container
* [ ] DNS Updates currently disabled

## Configuration

**config/ddns.yaml:**
```yaml
# IP Provider to determine external IP
ip_providers:
  - name: ipify.org
    ipv4: https://api.ipify.org/
    ipv6: https://api6.ipify.org/

# DNS Provider to update given DNS records
# Atm just ISPConfig is supported.
dns_providers:
  - name: ispconfig.me
    type: ispconfig
    url: https://login.ispconfig.me
    user: USER
    pass: SECRET
    records:
      # DNS id for A record from ispconfig
      - id: 1337
        name: gw.ispconfig.me.
        type: A
      # DNS id for AAAA record from ispconfig
      - id: 31337
        name: gw.ispconfig.me.
        type: AAAA
```