from setuptools import setup

setup(
   name='ddns',
   version='v0.1.4',
   description='Dynamic DNS Daemon',
   author='Özgür Caner',
   author_email='ocaner@gmail.com',
   packages=['ddns'],  #same as name
)