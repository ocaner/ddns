from enum import Enum
from logging import INFO, Formatter, StreamHandler, getLogger


class GoFormatter(Formatter):

    """
    F0810 15:52:23.637510    1382 server.go:198] failed to load Kub
    """

    def __init__(self):
        super().__init__(
            '%(levelname)s' +
            '%(asctime)s.%(msecs)03d' +
            ' %(name)s %(process)d' +
            ' %(filename)s:%(lineno)d]' +
            ' %(message)s', datefmt='%m%d %H:%M:%S')

    def format(self, record):
        record.levelname = record.levelname[0]
        return super().format(record)


DEFAULT_LEVEL = INFO
LOG = getLogger('ddns')
LOG.setLevel(DEFAULT_LEVEL)

# create console handler and set level to debug
CONSOLE_HDLR = StreamHandler()
CONSOLE_HDLR.setLevel(DEFAULT_LEVEL)

# create formatter
# Formatter('%(levelname)s%(asctime)s - %(name)s %(message)s', datefmt='%m%d %H:%M:%S')
FORMATTER = GoFormatter()

# add formatter to ch
CONSOLE_HDLR.setFormatter(FORMATTER)
for hdlr in LOG.handlers:
    LOG.removeHandler(hdlr)
# add ch to logger
LOG.addHandler(CONSOLE_HDLR)


class RecordType(Enum):
    A = "A"
    AAAA = "AAAA"
    CNAME = "CNAME"
    TXT = "TXT"
    SRV = "SRV"
    MX = "MX"
    NS = "NS"


class Record:

    uid = None

    name = None

    typ = None

    value = None
