#!/usr/bin/env python3

from codecs import decode
from enum import Enum
from logging import INFO, Formatter, StreamHandler, getLogger
from subprocess import check_output
from time import time

from requests import get
from yaml import safe_load

from ddns import LOG, Record, RecordType
from prometheus_client import Counter, Gauge

CHECK = Counter("ddns_check", "Domain check", ["host", "type", "ip"])
CHECK_TIME = Gauge("ddns_check_time", "Domain check", ["host", "type", "ip"])
READY = Gauge("ddns_ready", "DDNS Daemon Ready")
RELOAD = Gauge("ddns_reload", "DDNS Daemon Ready")


class DDNSPlugin:

    @staticmethod
    def records(config):
        records = []

        for record in config["records"]:
            rec = Record()
            rec.uid = record.get("id")
            rec.name = record.get("name")

            if record["type"] == RecordType.A.name:
                rec.typ = RecordType.A
            elif record["type"] == RecordType.AAAA.name:
                rec.typ = RecordType.AAAA

            records.append(rec)

        return records

    client = None

    def __init__(self, config):
        pass

    def finalize(self):
        pass


class DDNSClient:

    def update(self, ipv4, ipv6=None):
        pass

    def create(self, rec_type, hostname, value, ttl=3600):
        pass


class IPProvider:

    name = None

    ipv4_url = None

    ipv6_url = None

    def __init__(self, name, ipv4_url, ipv6_url=None):
        self.name = name
        self.ipv4_url = ipv4_url
        self.ipv6_url = ipv6_url

    def get_ips(self):
        ipv4 = None
        ipv6 = None

        if self.ipv4_url:
            resp = get(self.ipv4_url)
            if resp.status_code == 200:
                ipv4 = resp.text

        if self.ipv6_url:
            resp = get(self.ipv6_url)
            if resp.status_code == 200:
                ipv6 = resp.text

        return (ipv4, ipv6)


class DDNSDaemon:

    @staticmethod
    def load_config(name="config/ddns.yaml"):
        LOG.info("loading config from {} ...".format(name))
        config = None

        try:
            with open(name, "r") as config_stream:
                config = safe_load(config_stream)

            LOG.info("successfully loaded config from {}".format(name))

            ip_providers = DDNSDaemon.load_ip_providers(config)
            plugins, clients = DDNSDaemon.load_plugins_and_clients(config)

            LOG.info("loaded ip providers: {}".format(ip_providers))
            LOG.info("loaded plugins: {}".format(plugins))
            LOG.info("loaded clients: {}".format(clients))

            return config, ip_providers, plugins, clients
        except Exception as error:
            LOG.error("failed to reload: {}".format(error))

    @staticmethod
    def load_ip_providers(config):
        ip_providers = []

        for provider in config.get("ip_providers", []):
            ip_providers.append(IPProvider(provider.get(
                "name"), provider.get("ipv4"), provider.get("ipv6")))

        return ip_providers

    @staticmethod
    def load_plugins_and_clients(config):
        plugins = []
        clients = []

        for config in config.get("dns_providers", []):
            if config["type"] == "ispconfig":
                from ispconfig import Plugin
                plugin = Plugin(config)
                plugins.append(plugin)
                clients.append(plugin.client)

        return (plugins, clients)

    config = None

    plugins = None

    clients = None

    is_ready = False

    last_run = False

    def __init__(self):
        self.reload(initial=True)

    def check_dns(self, ipv4=None, ipv6=None):
        LOG.info("checking dns {} | {} ...".format(ipv4, ipv6))

        result = False

        for client in self.clients:
            for record in client.records:
                cmd = ["/usr/bin/dig", record.typ.name,
                       record.name, "@8.8.8.8"]

                output = check_output(cmd)
                LOG.debug("CMD: {} => {}".format(" ".join(cmd), output))
                lines = decode(output, "utf-8").split("\n")

                value = None
                time = -1.0

                for line in lines:
                    if line.find(record.name) == 0:
                        parts = line.split()
                        value = parts[4]
                    elif line.find(";; Query time: ") == 0:
                        time = int(line.split(" ")[3], 10) / 1000.0

                LOG.debug("VALUE: {}".format(value))
                LOG.debug("TYPE: {} == A ? {}".format(
                    record.typ, record.typ.name == RecordType.A.name))
                LOG.debug("TYPE: {} == AAAA ? {}".format(
                    record.typ, record.typ.name == RecordType.AAAA.name))

                CHECK.labels(record.name, record.typ.name, value).inc()
                CHECK_TIME.labels(
                    record.name, record.typ.name, value).set(time)

                if record.typ.name == RecordType.A.name:
                    if ipv4 != value:
                        LOG.info(
                            "IPV4 changed {} != {} (DNS)".format(ipv4, value))
                    else:
                        LOG.info("IPV4 unchanged {}".format(ipv4))
                        result = True
                elif record.typ.name == RecordType.AAAA.name:
                    if ipv6 != value:
                        LOG.info(
                            "IPV6 changed {} != {} (DNS)".format(ipv6, value))
                    else:
                        LOG.info("IPV6 unchanged {}".format(ipv6))
                        result = True
        return result

    def reload(self, initial=False):

        if initial:
            LOG.info("loading config")
        else:
            LOG.info("reloading config")

        self.is_ready = False
        start = time()
        config, ip_providers, plugins, clients = DDNSDaemon.load_config()
        self.config = config
        self.ip_providers = ip_providers
        self.plugins = plugins
        self.clients = clients
        self.is_ready = True
        delta = time() - start

        if initial:
            LOG.info("successfully loaded config in {}s".format(delta))
        else:
            LOG.info("successfully reloaded config in {}s".format(delta))

        return True

    def ready(self):
        LOG.info("ready {}".format(self.is_ready))
        return self.is_ready

    def health(self):
        if not self.is_ready:
            LOG.error("NOT READY")
            raise AssertionError("NOT READY")
            return False

        return self.last_run

    def run(self):
        LOG.info("running update cycle ...")
        start = time()
        result = True
        LOG.info("checking ips ...")
        for provider in self.ip_providers:
            ipv4, ipv6 = provider.get_ips()
            LOG.info("provider {} => {} | {}".format(
                provider.name, ipv4, ipv6))
            result = result and self.check_dns(ipv4, ipv6)

        self.last_run = result
        delta = time() - start

        LOG.info("update cycle completed in {}s with {}".format(
            delta, "success" if result else "failure"))

        return result

    def exit(self):
        for plugin in self.plugins:
            plugin.finalize()
