#!/usr/bin/env python3

import sys
from atexit import register
from threading import Event, Lock, Thread, Timer

from ddns import LOG
from ddns.core import READY, RELOAD, DDNSDaemon
from flask import Flask, request
from prometheus_client import Info, make_wsgi_app

LOG.info(r"""
________                              .__         ________    _______    _________
\______ \ ___.__. ____ _____    _____ |__| ____   \______ \   \      \  /   _____/
|    |  <   |  |/    \\__  \  /     \|  |/ ___\   |    |  \  /   |   \ \_____  \ 
|    `   \___  |   |  \/ __ \|  Y Y  \  \  \___   |    `   \/    |    \/        \
/_______  / ____|___|  (____  /__|_|  /__|\___  > /_______  /\____|__  /_______  /
    \/\/         \/     \/      \/        \/          \/         \/        \/ 

""")


i = Info('ddnsd', 'Dynamic DNS Daemon')
i.info({'version': '0.1.0', 'buildhost': 'horse@power'})

daemon = DDNSDaemon()


POOL_TIME = 600  # Seconds

# variables that are accessible from anywhere
threadLocals = {'daemon': daemon, 'LOG': LOG}
# lock to control access to variable
dataLock = Lock()
# thread handler


class DDNSThread(Thread):

    def __init__(self, *args, **kwargs):
        super(DDNSThread, self).__init__(*args, **kwargs)
        self._stop = Event()

    # function using _stop function
    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()


ddnsThread = DDNSThread()


def interrupt():
    threadLocals["LOG"].info("exiting...")
    threadLocals["daemon"].exit()
    ddnsThread.stop()
    ddnsThread.join()
    sys.exit(0)


def mainloop():
    with dataLock:
        # Do your stuff with commonDataStruct Here
        threadLocals["LOG"].info("calling run")
        threadLocals["daemon"].run()
        threadLocals["LOG"].info("called run")

    # Set the next thread to happen
    ddnsThread = Timer(POOL_TIME, mainloop, ())
    ddnsThread.start()


def startmainloop():
    # Create your thread
    daemon.run()
    ddnsThread = Timer(POOL_TIME, mainloop, ())
    ddnsThread.start()


# Initiate
startmainloop()
# When you kill Flask (SIGTERM), clear the trigger for the next thread
register(interrupt)

app = Flask(__name__)

# ui
@app.route('/')
def index():
    # threadLocals["daemon"]
    pass

# webroutes
@app.route('/metrics')
def metrics():
    LOG.info("metrics")
    return make_wsgi_app()


@app.route('/ready')
def ready():
    LOG.info("ready")

    result = daemon.ready()

    if result:
        READY.set(1.0)
    else:
        READY.set(0.0)

    return 'ready {}'.format(result), 200 if result else 500


@app.route('/reload')
def reload():
    LOG.info("reload")

    result = daemon.reload()

    if result:
        RELOAD.inc(1.0)
    else:
        RELOAD.set(0.0)

    return 'reload {}'.format(result), 200 if result else 500


@app.route('/healthz')
def healthz():
    LOG.info("healthz")
    result = daemon.health()
    return 'healthz {}'.format(result), 200 if result else 500


@app.route('/exit')
def exit():
    LOG.info("exit")
    daemon.exit()
    return 'exit'
