#!/usr/bin/env python3

from collections import OrderedDict
from html.parser import HTMLParser

from requests import Session, get, post

from ddns import LOG, RecordType
from ddns.core import DDNSClient, DDNSPlugin


class FormsParser(HTMLParser):
    data = False

    base = None

    forms = {}

    currentForms = []

    @staticmethod
    def attrs_to_dict(attrs):
        a = {}
        for k, v in attrs:
            a[k] = v

        return a

    def get_form_name(self, attrs):
        defv = str(len(self.forms.keys()))

        if not attrs:
            self.currentForm = defv
            return defv

        return attrs.get("name", attrs.get("id", defv))

    def get_current_form(self, a=None):
        LOG.debug("CURRENT {} => {}".format(self.currentForms, a))
        if not self.currentForms:
            self.currentForms.append(self.get_form_name(a))

        return self.currentForms[-1]

    def add_form(self, action=None, method="GET", fields={}):
        self.forms[self.get_current_form()] = dict(
            method=method, action=action or self.base, fields=fields)

    def handle_starttag(self, tag, attrs):
        if tag in ("form", ):
            #print("FORM {} @ {}".format(tag, attrs))
            a = FormsParser.attrs_to_dict(attrs)

            if "action" in a and not a["action"] and self.base:
                a["action"] = self.base

            self.currentForms.append(self.get_form_name(a))

            self.add_form(method=a["method"], action=a["action"])

            LOG.debug("FORM: {}".format(a))

        if tag in ("input", ):
            a = FormsParser.attrs_to_dict(attrs)
            LOG.debug("INPUT: {}".format(a))
            LOG.debug("FORMS: {}".format(self.forms))
            LOG.debug("FORM : {}".format(self.get_current_form()))
            if "name" in a and "value" in a:
                idx = self.get_current_form()

                if idx not in self.forms:
                    self.add_form()

                self.forms[idx]["fields"][a["name"]] = a["value"]

                LOG.debug("IDX: {} => {}".format(
                    idx, self.forms[idx]["fields"]))

            self.data = True

    def handle_endtag(self, tag):
        if tag in ("input", ):
            #print("Encountered an end tag :", tag)
            self.data = False

        if tag in ("form", ):
            self.currentForms.pop()
            pass

    def handle_data(self, data):
        if self.data:
            #print("Encountered some data  :", data)
            pass


class ISPConfigDDNSClient(DDNSClient):

    def __init__(self, url, user, password, records=None):
        self.debug = False
        self.url = url
        self.user = user
        self.password = password
        self.records = records
        self.session = None

    def prepare(self, req):
        pass

    def login(self):
        if self.session:
            LOG.error("session is not None")
            raise AssertionError("session is not None, call logout first")

        self.session = Session()

        url = "{}/login/".format(self.url)
        resp = self.session.get(url)

        LOG.debug("RESP   : {}".format(resp))
        LOG.debug("SESSION: {}".format(self.session.cookies))

        data = dict(s_mod="login", s_pg="index",
                    username=self.user, password=self.password)
        url = "{}/login/index.php".format(self.url)
        resp = self.session.post(url, data)

        LOG.debug("RESP   : {}".format(resp))
        LOG.debug("SESSION: {}".format(self.session.cookies))

        if resp.status_code == 200:
            LOG.info("ispconfig login {} succeeded".format(self.url))
        else:
            LOG.error("ispconfig login {} failed".format(self.url))

    def logout(self):
        resp = self.session.get("{}/login/logout.php".format(self.url))

        LOG.debug("RESP   : {}".format(resp))
        LOG.debug("SESSION: {}".format(self.session.cookies))

        if resp.status_code == 200:
            LOG.info("ispconfig {} logout succeeded".format(self.url))
        else:
            LOG.error("ispconfig {} logout failed".format(self.url))

        self.session = None

    def update(self, ipv4, ipv6=None):
        print("Update ip {} / {}".format(ipv4, ipv6))

    def dns_update(self, record_type, record_id, value, ttl=600):
        url = "{}/dns/dns_{}_edit.php?id={}".format(
            self.url, record_type.name.lower(), record_id)

        LOG.debug("URL    : {}".format(url))

        resp = self.session.get(url)

        LOG.debug("XXX UPDATE XXX")
        LOG.debug("RESP   : {}".format(resp))
        LOG.debug("SESSION: {}".format(self.session.cookies))
        parser = FormsParser()
        parser.base = url
        parser.feed(resp.text)

        for k, form in parser.forms.items():
            LOG.debug("FORM : {}".format(form))
            data = form["fields"]

            data["data"] = value

            LOG.debug("DATA: {}".format(data))

            resp = self.session.post(url, data)

            LOG.debug("RESP   : {}".format(resp))
            LOG.debug("SESSION: {}".format(self.session.cookies))
            break

    def __repr__(self):
        return "ispconfig {}".format(self.url)


class Plugin(DDNSPlugin):
    name = "ispconfig"

    def __init__(self, config):
        self.config = config
        self.client = ISPConfigDDNSClient(
            self.config.get("url"),
            self.config.get("user"),
            self.config.get("pass"),
            records=DDNSPlugin.records(self.config)
        )
        self.client.login()

    def __repr__(self):
        return "ispconfig"

    def finalize(self):
        self.client.logout()
