FROM alpine

LABEL name "ddns"
LABEL author "Özgür Caner <ocaner@gmail.com>"
LABEL description "Dynamic DNS daemon"

COPY requirements.txt core.py ddns.py ispconfig.py /opt/ddns/

RUN apk update \
    && apk add python3 bind-tools \
    && cd /opt/ddns \
    && pip3 install -r requirements.txt

WORKDIR /opt/ddns
ENV FLASK_APP=ddns.py
ENTRYPOINT waitress-serve --port=5000 --ipv4 --ipv6 --threads=1 'ddns:app'